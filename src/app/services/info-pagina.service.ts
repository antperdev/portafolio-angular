import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina } from '../interfaces/info-pagina.interface';
import { InfoEquipo } from '../interfaces/info-equipo.interface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  cargada: boolean = false;

  equipo: any[] = [];

  equipoCargada: boolean = false;

  constructor(private http: HttpClient) {

    // console.log("Servicio de infopágina listo");

    //Leer el archivo JSON
    this.cargarInfo();

    this.cargarEquipo();

  }

  private cargarInfo() {
    //Leer el archivo JSON
    this.http.get('assets/data/data-pagina.json')
      .subscribe((resp: InfoPagina) => {
        this.cargada = true;
        this.info = resp;

        //console.log(resp);
        // console.log(resp['email']);
      }
      );

  }

  private cargarEquipo() {
    //Leer el archivo JSON
    this.http.get('https://angular-html-5c002-default-rtdb.europe-west1.firebasedatabase.app/equipo.json')
      .subscribe((resp: any) => {
        this.equipoCargada = true;
        this.equipo = resp;

        // console.log(resp);
        // console.log(resp['email']);
      }
      );

  }
}
